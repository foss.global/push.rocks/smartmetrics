import * as plugins from './smartmetrics.plugins.js';
import * as interfaces from './smartmetrics.interfaces.js';

export class SmartMetrics {
  public started = false;
  public sourceNameArg: string;
  public logger: plugins.smartlog.Smartlog;
  public registry: plugins.promClient.Registry;
  public maxMemoryMB: number;

  public async setup() {
    const collectDefaultMetrics = plugins.promClient.collectDefaultMetrics;
    this.registry = new plugins.promClient.Registry();
    collectDefaultMetrics({ register: this.registry });
  }

  constructor(loggerArg: plugins.smartlog.Smartlog, sourceNameArg: string) {
    this.logger = loggerArg;
    this.sourceNameArg = sourceNameArg;
    this.setup();
    this.checkMemoryLimits();
  }

  private checkMemoryLimits() {
    let heapStats = plugins.v8.getHeapStatistics();
    let maxHeapSizeMB = heapStats.heap_size_limit / 1024 / 1024;
    let totalSystemMemoryMB = plugins.os.totalmem() / 1024 / 1024;

    let dockerMemoryLimitMB = totalSystemMemoryMB;
    try {
      let dockerMemoryLimitBytes = plugins.fs.readFileSync(
        '/sys/fs/cgroup/memory/memory.limit_in_bytes',
        'utf8'
      );
      dockerMemoryLimitMB = parseInt(dockerMemoryLimitBytes, 10) / 1024 / 1024;
    } catch (error) {
      // Ignore - this will fail if not running in a Docker container
    }

    // Set the maximum memory to the lower value between the Docker limit and the total system memory
    this.maxMemoryMB = Math.min(totalSystemMemoryMB, dockerMemoryLimitMB, maxHeapSizeMB);

    // If the maximum old space size limit is greater than the maximum available memory, throw an error
    if (maxHeapSizeMB > this.maxMemoryMB) {
      throw new Error('Node.js process can use more memory than is available');
    }
  }

  public start() {
    const unattendedStart = async () => {
      if (this.started) {
        return;
      }
      this.started = true;
      while (this.started) {
        this.logger.log('info', `sending heartbeat for ${this.sourceNameArg} with metrics`, {
          eventType: 'heartbeat',
          metrics: await this.getMetrics(),
        });
        await plugins.smartdelay.delayFor(20000, null, true);
      }
    };
    unattendedStart();
  }

  public formatBytes(bytes: number, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  public async getMetrics() {
    const originalMetrics = await this.registry.getMetricsAsJSON();
    const pids = await plugins.pidtree(process.pid);
    const stats = await plugins.pidusage([process.pid, ...pids]);

    let cpuPercentage = 0;
    for (const stat of Object.keys(stats)) {
      if (!stats[stat]) continue;
      cpuPercentage += stats[stat].cpu;
    }
    let cpuUsageText = `${Math.round(cpuPercentage * 100) / 100} %`;

    let memoryUsageBytes = 0;
    for (const stat of Object.keys(stats)) {
      if (!stats[stat]) continue;
      memoryUsageBytes += stats[stat].memory;
    }

    let memoryPercentage =
      Math.round((memoryUsageBytes / (this.maxMemoryMB * 1024 * 1024)) * 100 * 100) / 100;
    let memoryUsageText = `${memoryPercentage}% | ${this.formatBytes(
      memoryUsageBytes
    )} / ${this.formatBytes(this.maxMemoryMB * 1024 * 1024)}`;

    console.log(`${cpuUsageText} ||| ${memoryUsageText} `);

    const returnMetrics: interfaces.IMetricsSnapshot = {
      process_cpu_seconds_total: (
        originalMetrics.find((metricSet) => metricSet.name === 'process_cpu_seconds_total') as any
      ).values[0].value,
      nodejs_active_handles_total: (
        originalMetrics.find((metricSet) => metricSet.name === 'nodejs_active_handles_total') as any
      ).values[0].value,
      nodejs_active_requests_total: (
        originalMetrics.find(
          (metricSet) => metricSet.name === 'nodejs_active_requests_total'
        ) as any
      ).values[0].value,
      nodejs_heap_size_total_bytes: (
        originalMetrics.find(
          (metricSet) => metricSet.name === 'nodejs_heap_size_total_bytes'
        ) as any
      ).values[0].value,
      cpuPercentage,
      cpuUsageText,
      memoryPercentage,
      memoryUsageBytes,
      memoryUsageText,
    };
    return returnMetrics;
  }

  public stop() {
    this.started = false;
  }
}
